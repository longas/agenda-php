<?php
  include 'db.php';

  $orden = "";
  $modo = "";

  // Creamos el query para ordenar el listado
  if (isset($_GET['ordenar']))
  {
    $ordenar = $_GET['ordenar'];
    $orden = " ORDER BY LOWER(" . $ordenar . ")";
  }

  if (isset($_GET['modo']) && $_GET['modo'] == 'desc') {
    $modo = " " . $_GET['modo'];
  }

  $query = "SELECT * FROM contactos" . $orden . $modo;
  $result = $db->query($query);

  include 'templates/header.php';
?>

<table cellspacing="0" cellpadding="0">
  <tr>
    <th><a href="?ordenar=nombre&modo=desc">↓</a> Nombre <a href="?ordenar=nombre&modo=asc">↑</a></th>
    <th><a href="?ordenar=apellidos&modo=desc">↓</a> Apellidos <a href="?ordenar=apellidos&modo=asc">↑</a></th>
    <th><a href="?ordenar=telefono&modo=desc">↓</a> Teléfono <a href="?ordenar=telefono&modo=asc">↑</a></th>
    <th><a href="?ordenar=correo&modo=desc">↓</a> Correo <a href="?ordenar=correo&modo=asc">↑</a></th>
  </tr>
    
<?php
  foreach($result as $row)
  {
    echo "<tr>";
    echo "<td>" . $row['nombre'] . "</td>";
    echo "<td>" . $row['apellidos'] . "</td>";
    echo "<td>" . $row['telefono'] . "</td>";
    echo "<td>" . $row['correo'] . "</td>";
    echo "</tr>";
  }
  echo "</table>";

  // Cierro la conexión
  $db = null;

  include 'templates/footer.php';
?>