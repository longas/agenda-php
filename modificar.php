<?php
include 'db.php';

include 'templates/header.php';

$result = $db->query('SELECT * FROM contactos');
?>

<form method='POST' action='modificar_form.php'>
  <table cellspacing="0" cellpadding="0">
    <tr>
      <th>Modificar</th>
      <th>Nombre</th>
      <th>Apellidos</th>
      <th>Teléfono</th>
      <th>Correo</th>
    </tr>

<?php
  foreach($result as $row)
  {
    echo "<tr>";
    echo "<td><input type='radio' name='contacto' value='" .  $row['id'] . "'></td>";
    echo "<td>" . $row['nombre'] . "</td>";
    echo "<td>" . $row['apellidos'] . "</td>";
    echo "<td>" . $row['telefono'] . "</td>";
    echo "<td>" . $row['correo'] . "</td>";
    echo "</tr>";
  }
  echo "</table>
  <input type='submit' value='Modificar'>
  </form>";

  // Cierro la conexión
  $db = null;

  include 'templates/footer.php';
?>