<?php
include 'db.php';
include 'contacto.php';

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
  $id = $nombre = $apellidos = $tlf = $email = "";

  if (!empty($_POST["nombre"]) && !empty($_POST["apellidos"]) && !empty($_POST["tlf"]) && !empty($_POST["email"]))
  {
    $id = $_POST["id"];
    $nombre = $_POST["nombre"];
    $apellidos = $_POST["apellidos"];
    $tlf = $_POST["tlf"];
    $email = $_POST["email"];

    $contacto = new Contacto($nombre, $apellidos, $tlf, $email);
    $contacto->modificar($db, $id);
  }
}

// Cierro la conexión
$db = null;

header('location: index.php');
?>