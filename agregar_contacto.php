<?php
include 'db.php';
include 'contacto.php';

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
  $nombre = $apellidos = $tlf = $email = "";

  if (!empty($_POST["nombre"]) && !empty($_POST["apellidos"]) && !empty($_POST["tlf"]) && !empty($_POST["email"])) {
    $nombre = $_POST["nombre"];
    $apellidos = $_POST["apellidos"];
    $tlf = $_POST["tlf"];
    $email = $_POST["email"];

    $contacto = new Contacto($nombre, $apellidos, $tlf, $email);
    $contacto->guardar($db);
  }
}

// Cierro la conexión
$db = null;

header('location: index.php');
?>