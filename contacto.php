<?php

class Contacto
{
  public $nombre;
  public $apellidos;
  public $telefono;
  public $correo;

  public function __construct($nombre, $apellidos, $telefono, $correo)
  {
    $this->nombre = $nombre;
    $this->apellidos = $apellidos;
    $this->telefono = $telefono;
    $this->correo = $correo;
  }

  public function guardar($db)
  {
    try
    {
      $datos = array('nombre' => $this->nombre,
                    'apellidos' => $this->apellidos,
                    'telefono' => $this->telefono,
                    'correo' => $this->correo);

      $insert = "INSERT INTO contactos (nombre, apellidos, telefono, correo) 
                  VALUES (:nombre, :apellidos, :telefono, :correo)";
      $stmt = $db->prepare($insert);
      $stmt->execute($datos);

    } catch(PDOException $e)
    {
      echo $e->getMessage();
    }
  }

  public function modificar($db, $id)
  {
    try
    {
      $datos = array('nombre' => $this->nombre,
                    'apellidos' => $this->apellidos,
                    'telefono' => $this->telefono,
                    'correo' => $this->correo,
                    'id' => $id);

      $update = "UPDATE contactos SET nombre=:nombre, apellidos=:apellidos, telefono=:telefono, correo=:correo WHERE id=:id";
      $stmt = $db->prepare($update);
      $stmt->execute($datos);

    } catch(PDOException $e)
    {
      echo $e->getMessage();
    }
  }
}

?>