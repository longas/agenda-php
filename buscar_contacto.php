<?php
  include 'db.php';

  /*Creamos el sistema para poder usar los datos del GET
  para añadirle las opciones de ordenar*/
  $query = "SELECT * FROM contactos WHERE ";
  $getQuery = "";
  $datos = array();
  $primero = true;

  // Si no se ha rellenado nada se redirige al index
  if (empty($_GET['nombre']) && empty($_GET['apellidos']) && empty($_GET['telefono']) && empty($_GET['correo']))
  {
    header('location: index.php');
  }

  // Creamos el SELECT FROM WHERE
  foreach ($_GET as $key => $value)
  {
    if (!empty($value) && $key != 'ordenar' && $key != 'modo')
    {
      if(!$primero)
      {
        $query .= " AND";
      }

      $query .= " " . $key . " LIKE :" . $key . "";
      $getQuery .= "&" . $key ."=" . $value;
      $datos[$key] = '%'.$value.'%';

      $primero = false;
    }
  }

  // Añadimos el ORDER BY
  if (isset($_GET['ordenar']))
  {
    $ordenar = $_GET['ordenar'];
    $query .= " ORDER BY LOWER(" . $ordenar . ")";
  }

  if (isset($_GET['modo']) && $_GET['modo'] == 'desc') {
    $query .= " " . $_GET['modo'];
  }

  // Buscamos en la bbdd por los campos rellenados
  try
  {
    $result = $db->prepare($query);
    $result->execute($datos);
  } catch(PDOException $e)
  {
    echo $e->getMessage();
  }

  include 'templates/header.php';
?>

<table cellspacing="0" cellpadding="0">
  <tr>
    <th><a href="?ordenar=nombre&modo=desc<?php echo $getQuery?>">↓</a> Nombre <a href="?ordenar=nombre&modo=asc<?php echo $getQuery?>">↑</a></th>
    <th><a href="?ordenar=apellidos&modo=desc<?php echo $getQuery?>">↓</a> Apellidos <a href="?ordenar=apellidos&modo=asc<?php echo $getQuery?>">↑</a></th>
    <th><a href="?ordenar=telefono&modo=desc<?php echo $getQuery?>">↓</a> Teléfono <a href="?ordenar=telefono&modo=asc<?php echo $getQuery?>">↑</a></th>
    <th><a href="?ordenar=correo&modo=desc<?php echo $getQuery?>">↓</a> Correo <a href="?ordenar=correo&modo=asc<?php echo $getQuery?>">↑</a></th>
  </tr>
    
<?php
  foreach($result as $row)
  {
    echo "<tr>";
    echo "<td>" . $row['nombre'] . "</td>";
    echo "<td>" . $row['apellidos'] . "</td>";
    echo "<td>" . $row['telefono'] . "</td>";
    echo "<td>" . $row['correo'] . "</td>";
    echo "</tr>";
  }
  echo "</table>";

  // Cierro la conexión
  $db = null;

  include 'templates/footer.php';
?>