<?php

try
{
  $db = new PDO('sqlite:agenda.sqlite3');
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $db->exec("CREATE TABLE IF NOT EXISTS contactos (
            id INTEGER PRIMARY KEY,
            nombre VARCHAR(40),
            apellidos VARCHAR(60), 
            telefono VARCHAR(10),
            correo VARCHAR(50)
            )");
}
catch(PDOException $e)
{
  echo $e->getMessage();
}

?>