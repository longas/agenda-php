<?php
include 'db.php';

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['contactos']))
{
  // Guardamos el array de los contactos seleccionados
  $contactos = $_POST['contactos'];

  if (!empty($contactos))
  {
    // Iteramos por cada uno y lo borramos
    for($i = 0; $i < count($contactos); $i++)
    {
      $stmt = $db->prepare("DELETE FROM contactos WHERE id=:id");
      $stmt->bindParam(':id', $contactos[$i]);
      $stmt->execute();
    }
  }
}

// Cierro la conexión
$db = null;

header('location: index.php');
?>