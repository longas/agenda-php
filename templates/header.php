<!DOCTYPE html>
<html>
<head>
  <title>Agenda | Gabriel Longás</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="./static/style.css">
</head>
<body>
  <ul class="menu">
    <li><a href="index.php">Lista</a></li>
    <li><a href="agregar.php">Añadir</a></li>
    <li><a href="modificar.php">Modificar</a></li>
    <li><a href="buscar.php">Buscar</a></li>
    <li><a href="borrar.php">Borrar</a></li>
    <li><a href="limpiar.php">Borrar todo</a></li>
  </ul>