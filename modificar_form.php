<?php
include 'db.php';
include 'contacto.php';

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['contacto']))
{
  // Buscamos el contacto con la id seleccionada anteriormente
  $stmt = $db->prepare('SELECT * FROM contactos WHERE id = :id');
  $stmt->bindParam(':id', $_POST['contacto']);
  $stmt->execute();

  // Creamos un contacto para luego poner sus datos en el formulario
  foreach($stmt as $row)
  {
    $contacto = new Contacto($row['nombre'], $row['apellidos'], $row['telefono'], $row['correo']);
  }

  include 'templates/header.php';
?>

  <form method="POST" action="modificar_contacto.php">
    <input type="hidden" name="id" value="<?php echo $_POST['contacto'] ?>">
    <p>Nombre:
    <input type="text" name="nombre" value="<?php echo $contacto->nombre ?>"></p>
    <p>Apellidos:
    <input type="text" name="apellidos" value="<?php echo $contacto->apellidos ?>"></p>
    <p>Teléfono:
    <input type="text" name="tlf" value="<?php echo $contacto->telefono ?>"></p>
    <p>Email:
    <input type="text" name="email" value="<?php echo $contacto->correo ?>"></p>
    <input type="submit" name="submit" value="Guardar">
  </form>

<?php
  include 'templates/footer.php'; 
  
  // Cierro la conexión
  $db = null;

} else
{
  header('location: index.php');
}
?>